(ns digital-lending.core
  (:require
    [ring.middleware.reload :refer [wrap-reload]])
  (:use
    [compojure.route :only [files not-found]]
    [compojure.core :refer :all]
    [compojure.handler :as handler]
    org.httpkit.server
    digital-lending.products
    digital-lending.customer)
  (:gen-class)
  (:import (org.joda.time DateTime)))

(defn #^DateTime now []
  (DateTime.))

(def server-start-time (atom 0))

(defn init-server
  "Function defines server once"
  []
  (defonce server (atom nil)))

(defn initialize-params
  "We initialize the app with sample data for test"
  []
  ;; initialize server
  (init-server)
  ;create customers by passing [mobile wallet-bal max-allowable loan-bal]
  (new-customer 710123 0 20000 0)
  (new-customer 710456 0 20000 0)
  ;create products with its properties
  (new-product "A" 10000 0.1 "15 days")
  (new-product "B" 25000 0.125 "30 days"))

(declare process-request)
(defroutes all-routes
           (POST "/customer" req (process-request req))
           (GET "/customer" req (process-request req))
           (not-found {:status 404
                       :headers {"Content-Type" "text/plain"}
                       :body "Page not found"}))

;;; API handler
(def app (handler/api all-routes))

(defn start-server
  "Function starts server."
  []
  (try
    (do
      (reset! server (run-server (-> #'app (wrap-reload)) {:port 5000
                                                           :thread 1
                                                           :worker-name-prefix "digital-lending"})))
    (catch Exception e
      (print (format "!startServer(%s)" e)))))

(defn end-program
  "Function shuts application down gracefully"
  []
  ; stop server
  (when-not (nil? @server)
    (@server :timeout 100)
    (reset! server nil)))

(defn -main
  "Main function"
  [& args]
  (reset! server-start-time (now))
  ;init required params
  (initialize-params)
  ;; register runtime hook
  (.addShutdownHook (Runtime/getRuntime) (Thread. end-program))
  (try
    (start-server)
    (catch Exception e
      (end-program))))

;; Implementation

(defn validate-lend
  "Validates if a customer is qualified the lend else reject"
  [mobile product]
  (let [mobile (keyword (str mobile))
        qualified (:qualification (mobile @customers))
        product-cost (:cost ((keyword product) @products))]
    (if (>= qualified product-cost)
      ; only take loans that are less than or equal your qualification
      (let [{:keys [cost interest tenure]} ((keyword product) @products)
            fee (* cost interest)]
        (create-new-loan mobile (+ cost fee) tenure))
      "You don't qualify for this product")
    ))

(defn process-request
  "docstring"
  [req]
  ;(println req)
  (let [{:keys [action mobile amount product]} (:params req)]
    (condp = (keyword action)
      :products (get-products)
      :lend (validate-lend mobile product)
      :pay (pay-loan (Integer/parseInt mobile) (Integer/parseInt amount))
      :walletbalance (wallet-balance (Integer/parseInt mobile))
      :loanbalance (loan-balance (Integer/parseInt mobile))
      "Action not supported"))
  )