(ns digital-lending.products
  (:require [clojure.data.json :as json]))

(def products (atom {}))

(let [product (create-struct :cost :interest :tenure)]
  (defn new-product
    "Creates a new product with its properties"
    [p-name p-cost p-interest p-tenure]
    (swap! products conj {(keyword (str p-name))
                           (struct product p-cost p-interest p-tenure)}))

  (defn get-products
    "Returns all the available products in json"
    []
    (json/write-str @products))
  
  (defn update-interest
    "Updates the interest of a product if it exist"
    [p-name new-interest]
    (if ((keyword p-name) @products)
      (do
        (reset! products
                (update-in @products [(keyword p-name)] assoc  :interest (* new-interest 100)))
        (str "Product " p-name " interest updated successfully"))
      "Please add the product first"))

  (defn update-cost
    "Updates the interest of a product if it exist"
    [p-name new-cost]
    (if ((keyword p-name) @products)
      (do
        (reset! products
                (update-in @products [(keyword p-name)] assoc  :cost new-cost))
        (str "Product " p-name " cost updated successfully"))
      "Please add the product first"))

  (defn update-tenure
    "Updates the interest of a product if it exist"
    [p-name new-tenure]
    (if ((keyword p-name) @products)
      (do
        (reset! products
                (update-in @products [(keyword p-name)] assoc  :tenure new-tenure))
        (str "Product " p-name " tenure updated successfully"))
      "Please add the product first"))

  )