(ns digital-lending.customer)

(def customers (atom {}))

(let [customer (create-struct :wallet :qualification :loan-balance)
      wallet (create-struct :wallet-balance )]

  (defn new-customer
    "Return a complete customer with wallet"
    [mobile wallet-bal max-allowable loan-bal]
    (swap! customers conj {(keyword (str mobile))
                           (struct customer (struct wallet wallet-bal) max-allowable loan-bal)}))
  
  (defn update-qualification
    "sets the new qualification of a customer"
    [mobile new-qualify]
    (if (nil? (get-in @customers [(keyword (str mobile))]))
      "Please add the customer first"
      (do
        (reset! customers
                (update-in @customers [(keyword (str mobile))] assoc  :qualification new-qualify))
        "Customer qualification has been updated successfully")))

  (defn create-new-loan
    "creates new loan only if you don't have a loan"
    [mobile loan-amount tenure]
    (let [loan (get-in @customers [(keyword (str mobile)) :wallet :loan-amount])]
      (if (or (nil? loan) (zero? loan))
        (do
          (reset! customers
                  (update-in @customers [(keyword (str mobile)) :wallet]
                             assoc :loan-amount loan-amount :tenure tenure))
          "You have successfully taken a loan")
        "Please pay your existing loan")))

  (defn pay-loan
    "pays a loan if it exists. overpayment is added to the wallet balance"
    [mobile paid]
    (let [balance (get-in @customers [(keyword (str mobile)) :wallet :loan-amount])]
      (if balance
        (if (> paid balance)
          (let [new-wallet (- paid balance)]
            (reset! customers
                    (update-in @customers [(keyword (str mobile)) :wallet]
                               assoc :wallet-balance new-wallet :loan-balance 0))
            "Loan paid in full and the overpayment has been added to your wallet balance")
          (do
            (reset! customers
                  (update-in @customers [(keyword (str mobile)) :wallet :loan-balance] - paid))
            "Loan paid partially. Thank you."))
        "You don't have a loan")))

  (defn wallet-balance
    "Returns the balance of the customer"
    [mobile]
    (str "You have a wallet balance of: " (get-in @customers [(keyword (str mobile)) :wallet :wallet-balance])))

  (defn loan-balance
    "Returns the balance of the customer"
    [mobile]
    (let [loan (get-in @customers [(keyword (str mobile)) :loan-balance])
          balance (if (nil? loan)
                    0 loan)]
      (str "You have a wallet balance of: " balance)))
  )