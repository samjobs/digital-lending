(defproject digital-lending "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [compojure "1.6.1"]
                 [ring/ring-defaults "0.3.2"]
                 [ring-middleware-format "0.7.2"]
                 [ring "1.6.3"]
                 [http-kit "2.3.0"]
                 [org.clojure/data.json "2.4.0"]
                 ]
  :main ^:skip-aot digital-lending.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
