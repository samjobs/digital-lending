# digital-lending

This is a microservice that mimics a lending platform. 
It provides an api that receives url encoded post and get requests. Based on the action parameter in the requests,
the service is able to 
1. List all products available
2. lend a customer any listed product
3. display balance
4. Add a customer
5. Add a product

At start up, the application will initialize two customers and two products for testing purposes. 
Additional products and customers can be added too.
## Compilation 

This service can be compiled to a jar by executing `lein uberjar` from the root of the project
## Usage

To run the app, the standalone library can be excuted as:
    `$ java -jar digital-lending-0.1.0-standalone.jar`

## Sample requests
Below are the sample request that can be process by the service:

```
curl 'http://127.0.0.1:5000/customer?action=products'
curl 'http://127.0.0.1:5000/customer?action=lend&mobile=710123&product=A'
curl 'http://127.0.0.1:5000/customer?action=pay&mobile=710123&amount=A'
curl 'http://127.0.0.1:5000/customer?action=walletbalance&mobile=710123'
```

